package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    
    ArrayList<CellState> grid;
    private CellState state;
    

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        
        grid = new ArrayList<CellState>();
        for (int row = 0; row<rows; row++){
            //grid.add(initialState);

            for (int col = 0; col < columns; col++){

                grid.add(initialState);
            }
        }
        

    
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        //this.state = element;
        if (row > numRows() || column > numColumns() || row < 0 || column < 0){
            throw new IndexOutOfBoundsException();
            
        }
        else{
        grid.set(indexOf(row, column), element);
        }
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row > numRows() || column > numColumns() || row < 0 || column < 0){
            throw new IndexOutOfBoundsException();
            
        }
        else{
            
            CellState st = grid.get(indexOf(row, column));
            return st;
        }
        
    }

    private int indexOf(int row, int column){
    
        return row*columns + column;
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        //this.rows = rows;
        //this.columns = columns;
        CellGrid gridC = new CellGrid(this.rows, this.columns, this.state);
        for (int row = 0; row<rows; row++){
            

            for (int col = 0; col < columns; col++){
                state = get(row, col);
                gridC.set(row, col, state);
            }
        }

        return gridC;
    }
    
}
