package cellular;

import java.security.cert.CertPathValidatorException;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
		
	}

	@Override
	public void initializeCells() {

		
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO

		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		System.out.println("step");
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row = 0; row < numberOfRows(); row++){
			for (int col = 0; col < numberOfColumns(); col++){
				//CellState state = getNextCell(row, col);
				
				
				try{
				CellState state = getNextCell(row, col);
				nextGeneration.set(row, col, state);
				}
				catch(Exception IndexOutOfBoundsException){
					System.out.println(col);
				}

				
				/*
				try{
				CellState state = getNextCell(row-1, col);
				
				nextGeneration.set(row, col, state);

				}
				catch(Exception IndexOutOfBoundsException){
					System.out.println(col);

				}
				try{
					CellState state = getNextCell(row+1, col);
					
					nextGeneration.set(row, col, state);
	
					}
					catch(Exception IndexOutOfBoundsException){
						System.out.println(col);
	
					}
					try{
						CellState state = getNextCell(row, col-1);
						
						nextGeneration.set(row, col, state);
		
						}
						catch(Exception IndexOutOfBoundsException){
							System.out.println(col);
		
						}

						try{
							CellState state = getNextCell(row, col+1);
							
							nextGeneration.set(row, col, state);
			
							}
							catch(Exception IndexOutOfBoundsException){
								System.out.println(col);
			
							}
				try{
				CellState state = getNextCell(row-1, col-1);
				
				nextGeneration.set(row, col, state);

				}
				catch(Exception IndexOutOfBoundsException){
					System.out.println(col);

				}
				try{
					CellState state = getNextCell(row+1, col-1);
					
					nextGeneration.set(row, col, state);
	
					}
					catch(Exception IndexOutOfBoundsException){
						System.out.println(col);
	
					}
					try{
						CellState state = getNextCell(row-1, col+1);
						
						nextGeneration.set(row, col, state);
		
						}
						catch(Exception IndexOutOfBoundsException){
							System.out.println(col);
		
						}

						try{
							CellState state = getNextCell(row+1, col+1);
							
							nextGeneration.set(row, col, state);
			
							}
							catch(Exception IndexOutOfBoundsException){
								System.out.println(col);
			
							}
							
							*/

				
			}
		}
		
		
		
		currentGeneration = nextGeneration;
			
        
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState status = currentGeneration.get(row, col);
		if (countNeighbors(row, col, CellState.ALIVE) < 2 && status == CellState.ALIVE){
			status = CellState.DEAD;
		}
		if (status == CellState.ALIVE && (countNeighbors(row, col, CellState.ALIVE) == 2  || countNeighbors(row, col, CellState.ALIVE) == 3) ){
			status = CellState.ALIVE;
		}

		if (countNeighbors(row, col, CellState.ALIVE) > 3 && status == CellState.ALIVE){
			status = CellState.DEAD;
		}
		if (countNeighbors(row, col, CellState.ALIVE) == 3 && status == CellState.DEAD){
			status = CellState.ALIVE;
		}

		
	
		/*else if ( status == CellState.ALIVE){
			status = CellState.DYING;
		}
		*/

		
		return status;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		
		
		if (row > 0 && getCellState(row-1, col)== state ){
			count++;
		}
	
	
		if (col > 0 && getCellState(row, col-1)== state){
			count++;
		}
		if (row < numberOfRows() && getCellState(row+1, col)== state){
			count++;
		}
		if (col < numberOfColumns() && getCellState(row, col+1)== state){
			count++;
		}
		if (row > 0 && col > 0 && getCellState(row-1, col-1)== state){
			count++;
		}
		if (col < numberOfColumns() && row > 0 && getCellState(row-1, col+1)== state ){
			count++;
		}
		if (row < numberOfRows() && col > 0 && getCellState(row+1, col-1)== state){
			count++;
		}
		if (col < numberOfColumns() && row < numberOfRows() && getCellState(row+1, col+1)== state){
			count++;
		}
		

		
		
	
		
		
		

		return count;
	}

	

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
